# An example about Bitbucket operations

You can reference an issue with:

	:::bash
		git commit -m 'Create a reference: see #1'

Declare that you are not going to fix it:

	:::bash
		git commit -m 'This is not broken wontfix #1'

Decide that it might be an issue:

	:::bash
		git commit -m 'Hold on a minute: holds #1'

Mark it as invalid:

	:::bash
		git commit -m "This doesn't make sense invalidate #1"

Reopen the issue:

	:::bash
		git commit -m 'Look, this is really a problem: reopen #1'

And finally close it for good:

	:::bash
		git commit -m 'We are all done, close issue 1'

You can have a look at [the issues](http://bitbucket.eduardoshanahan.com/bitbucket-example/issues)

And to the [issue history](http://bitbucket.eduardoshanahan.com/bitbucket-example/issue/1/example-change-issue-status-with-push) too.

Here is a [longer command list](http://www.eduardoshanahan.com/2013/09/03/how-to-change-an-issue-status-in-bitbucket-during-push/)